# webapp

#### 介绍
用户综合分析平台 --- 前端页面模块

#### 软件架构

前端页面模块（webapp）

​	Spring + SpringBoot + Layui + FastDFS + Echarts + EasyPOI + Eureka组件 + Ribbon组件 + Feign组件 + Config组件

后端功能接口模块（usermodel）

​	java业务模块：Spring + SpringBoot + MyBatis + MySQL + Redis + Eureka组件 + Ribbon组件 + Feign组件  + Config组件

大数据风险评估模块（EvaluateModel）

​	日志采集模块：Flume + Kafka + Sqoop 

​	云计算模块：

​		流计算：SparkStream + Flink

​		批处理：Hive + SparkSQL + FlinkTableAPI + MapperReduce + HDFS + ZooKeeper + Hbase

#### 安装教程

1. SpringCloud采用比较主流的版本：Greenwich.SR3
2. SpringBoot需要是2.1.x，采用：2.1.14.RELEASE
3. JDK采用：1.8
4. Maven采用：3.3.9
5. FastDFS分布式文件存储系统，需要安装FastDFS-Nginx模块，Nginx做为服务器，支持http协议访问 
6. 搭建大数据集群环境

#### 使用说明

1. 前端页面模块 <https://gitee.com/gao-yiguan/webapp> 
2. 后台功能接口模块 <https://gitee.com/gao-yiguan/usermodel> 
3. Eureka服务注册中心模块 <https://gitee.com/gao-yiguan/EurekaServer> 
4. Config注册中心模块 <https://gitee.com/gao-yiguan/ConfigServer> 
5. 配置文件仓库 <https://gitee.com/gao-yiguan/config> 
6. 大数据风险评估模块 <https://gitee.com/gao-yiguan/EvaluateModel> 

#### 实时业务

##### 注册风险

检测出一些不正常的注册行为，并产生注册评估报告，从而挖掘潜在风险

- 短时间内，同一设备在同一网络下注册数量超过N个
- 同一设备或者IP，规定时间内多次调用短信或者邮件服务超过N次

##### 登录风险

检测出非用户登录，或者账号被盗等潜在风险的挖掘

- 异地登录认定为有风险（不在用户经常登录地）
- 登录的位移速度超过正常值，认定有风险
- 更换设备，认定有风险
- 登录时段（习惯），出现了异常，存在风险
- 每天的累计登录次数超过上限，认定有风险
- 密码输入错误或者输入差异性比较大，认定有风险
- 如果用户的输入特征发生变化（输入每个控件所需时长（ms））,认定有风险

##### 交易风险

辅助系统检测用户支付环境、地区、网络、次数等

- 用户交易习惯，出现在异常时段认定有异常
- 检测用户的网络环境，如果不是经常使用的WIFI
- 短时间内产生多次交易，存在风险
- 支付金额过大，认定有风险
- 陌生地区支付，认定有风险
- 交易设备发生变化

##### 活动风险

活动风险涉及活动时间、奖励兑换（兑换率）

- 活动时间提前结束，认定有风险
- 活动短时间内兑换率过高，认定有风险
- 一个账号多次兑换，认定有风险

#### 离线业务

辅助流计算或者企业运营部更好地经营业务

- 分析平台用户成分（性别、设备、年龄段、地域等）
- 对App的使用习惯（时间、频道（类别））
- 频道或者类别的点击排行榜，页面的停留时长、跳出率
- 风控系统中各类评估因子在每个应用的表现
- 

对用户进行统计分析

- 用户的相关信息：年龄、性别.....
- 用户使用App的习惯：时间、使用的设备、地理位置
- 用户的订单金额
- 用户的访问习惯：类别、页面的停留时间

对用户分析统计之后，可以给用户画像。给用户贴标签。可以做针对性的推广营销

