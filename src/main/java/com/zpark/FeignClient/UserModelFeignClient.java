package com.zpark.FeignClient;

import com.zpark.common.R;
import com.zpark.config.FeignLoggerLevelConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "UserModelServer",configuration= FeignLoggerLevelConfig.class)
public interface UserModelFeignClient {

    @GetMapping("usermodel/user/loginUser")
    public R loginUser(@RequestParam("username") String username,@RequestParam("password") String password);

    @GetMapping("usermodel/user/queryUserByPage")
    public R queryUserByPage(@RequestParam("column") String column,@RequestParam("value") String value,@RequestParam("page") Integer page,@RequestParam("limit") Integer limit);

    @GetMapping("usermodel/user/queryUserById")
    public R queryUserById(@RequestParam("id") Integer id);

    @PostMapping("usermodel/user/addUser")
    public R addUser(@RequestBody Map map);

    @PostMapping("usermodel/user/amendUser")
    public R amendUser(@RequestBody Map map);

    @DeleteMapping("usermodel/user/dropUserById")
    public R dropUserById(@RequestParam("id") Integer id);

    @DeleteMapping("usermodel/user/dropUserByIds")
    public R dropUserByIds(@RequestBody Integer[] ids);

    @PostMapping("usermodel/user/addAllUser")
    public R addAllUser(@RequestBody String[] jsonList);

    @GetMapping("usermodel/user/queryUserByIds")
    public R queryUserByIds(@RequestBody Integer[] ids);

    @RequestMapping("usermodel/user/queryAllUser")
    public R queryAllUser();

    @GetMapping("usermodel/user/queryUserSexCount")
    public R queryUserSexCount();

    @GetMapping("usermodel/menu/queryAllMenus")
    public R queryAllMenus();
}
