package com.zpark.controller;


import com.zpark.FeignClient.UserModelFeignClient;
import com.zpark.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private UserModelFeignClient userModelFeignClient;

    @RequestMapping("/queryAllMenus")
    public R queryAllMenus(){
        return userModelFeignClient.queryAllMenus();
    }

}
