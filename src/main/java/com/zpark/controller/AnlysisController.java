package com.zpark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RestController
public class AnlysisController {


    @Autowired
    private RestTemplate restTemplate;

    private String url= "http://usermodel/";

    @RequestMapping("/chinaMap")
    public String chinaMap(String start,String end){
//        System.out.println(start+"****"+end);
        //如果start和end是空字符串，就应该是读取最近7天的数据；否则就应该是读取指定时间段的数据

        if("".equals(start)&& "".equals(end)){
            Date now = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            end = simpleDateFormat.format(now);

            //start =now-7天
            Calendar calendar = Calendar.getInstance();//当前时间
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            start=calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);

        }

        //把请求发送到后台完成对应的操作
        String json = restTemplate.getForObject(url + "chinaMap?start=" + start + "&end=" + end+"&appName=QQ", String.class);


        return json;
    }

    @RequestMapping("/provinceMap")
    public String provinceMap(String start,String end,String province){
        //如果start和end是空字符串，就应该是读取最近7天的数据；否则就应该是读取指定时间段的数据
        //根据省份读取到该省份下对应的数据

        if("".equals(start)&& "".equals(end)){
            Date now = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            end = simpleDateFormat.format(now);

            //start =now-7天
            Calendar calendar = Calendar.getInstance();//当前时间
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            start=calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);

        }

        //把请求发送到后台完成对应的操作
        String json = restTemplate.getForObject(url + "provinceMap?start=" + start + "&end=" + end+"&province="+province+"&appName=QQ", String.class);


        return json;
    }

    /*public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();//当前时间
        String start=calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(start);
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        System.out.println("=============================");
        start=calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(start);
    }*/
}
