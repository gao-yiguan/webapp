package com.zpark.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{
    @Excel(name = "用户编号")
    private Integer id;
    @Excel(name = "用户姓名")
    private String name;
    @Excel(name = "用户密码")
    private String password;
    @Excel(name = "用户性别",replace = {"男_1","女_0"})
    private Integer sex;
    //@Excel(name = "用户头像",type = 2,width = 32,height = 32)
    private String photo;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户生日",format = "yyyy-MM-dd")
    private Date birthDay;
    @Excel(name = "用户邮箱")
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(Integer id, String name, String password, Integer sex, String photo, Date birthDay, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.sex = sex;
        this.photo = photo;
        this.birthDay = birthDay;
        this.email = email;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", sex=" + sex +
                ", photo='" + photo + '\'' +
                ", birthDay=" + birthDay +
                ", email='" + email + '\'' +
                '}';
    }
}
