package com.zpark.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {
    /**
     * java.util.Date ---> String date
     * */
    public static String utilDateToStringDate(java.util.Date utilDate){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(utilDate);
            return date;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * String date ---> java.sql.Timestamp
     * */
    public static java.sql.Timestamp stringToTimestamp(String time){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date utilDate = sdf.parse(time);
            return new java.sql.Timestamp(utilDate.getTime());
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 获取当前系统时间（年月日 时分秒） 返回值String
     * */
    public static String getNowTimeString(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }
    /**
     * 获取当前系统时间（年月日 时分秒） 返回值java.sql.Timestamp
     * */
    public static java.sql.Timestamp getNowTimeTimestamp(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return stringToTimestamp(sdf.format(new Date()));
    }
    /**
     * java.util.Date ---> java.time.LocalDateTime
     * */
    public static LocalDateTime getNowTimeLocalDataTime(Date date){
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return localDateTime;
    }
}
