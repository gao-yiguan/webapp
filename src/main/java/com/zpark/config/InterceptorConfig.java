package com.zpark.config;

import com.zpark.interceptor.ForceLoginInterceptor;
import com.zpark.interceptor.UserInputInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new UserInputInterceptor()).addPathPatterns("/user/loginUser");
        registry.addInterceptor(new ForceLoginInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/css/**","/js/**","/layui/**","/login.html","/user/createCode","/user/loginUser");
    }
}
