package com.zpark.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Component
public class UserInputInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        String inputFeature="";
        for (Cookie cookie : cookies) {
            if("feature".equals(cookie.getName())){
                inputFeature=cookie.getValue();
                break;
            }
        }
        //获取到名字是User-Agent的请求头
        String header = request.getHeader("User-Agent");
        //获取到远程主机：用户的IP地址
        String remoteHost = request.getRemoteHost();

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println(username);
        System.out.println(password);
        System.out.println(inputFeature+"&&&"+header+"^^^"+remoteHost);
        //把cookie中传输过来的数据，通过%2C截开。
        //因为在页面中放入cookie的时候是通过逗号隔开的，逗号是不能在cookie中使用的，被转义成了%2C
        String[] inputFeatures = inputFeature.split("%2C");
        System.out.println(Arrays.toString(inputFeatures));

        //把用户名，密码，系统当前时间，IP地址，用户输入特征，用户使用的设备....以日志的方式，通过flume传输到kafka


        return true;
    }
}
