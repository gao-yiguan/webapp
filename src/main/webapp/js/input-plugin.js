$.fn.extend({
    //收集用户的表单输入特征
    inputFeatures:function () {
        //获取表单的所有input标签
        var inputs=$(this).find("input")
        //为所有的input绑定两个事件
        var futuresMap={}
        $.each(inputs,function (i,item) {
            //记录用户第一次在这个输入框里按下的时间
            $(item).bind("keydown",function (event) {
                var start=  $(this).data("start")//根据start这个key到这个input输入框中获取数据
                if(!start){
                    var start = new Date().getTime();//时间戳
                    $(this).data("start",start)
                }
            })
            //记录输入完成的时间
            $(item).bind("keyup",function (event) {
                var end = new Date().getTime();//按键弹起的时间戳
                var start= $(this).data("start")//获取到这个输入框开始输入内容的时间戳
                //当用户输入错误并没有删完的时候累计时间  删除完成重新输入时间归0
                if( $(this).val()==""){
                    //如果输入框中没有内容，把start设置为null
                    $(this).data("start",null)
                    futuresMap[i]= 0
                }else {
                    futuresMap[i] = end - start
                }
                console.log("第"+i+"个input输入框花费的时间是"+(end-start))
                //对key进行排序
                var keys=[]
                for(var k in futuresMap){
                    keys.push(k)
                }
                var sortedKeys = keys.sort();

                var feature = sortedKeys.map(function (key) { return futuresMap[key] })

                console.log(feature.join(","))
                //创建一个会话cookie
                $.cookie('feature', feature.join(","));
            })
        })
    }
})